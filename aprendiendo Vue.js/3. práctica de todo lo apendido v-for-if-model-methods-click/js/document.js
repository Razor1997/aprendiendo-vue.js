const App = new Vue({
    el: "#app",
    data: {
        persona: [
            { nombre: "Bryan Cometa", edad: "23", estatura: "1.80" },
            { nombre: "Zully Diaz", edad: "23", estatura: "1.52" }
        ],
        nombrePersona: "",
        edadPersona: "",
        estaturaPersona: "",
        buscaNombrePersona:"",
        indicePersona:0,
    },
    methods: {
        guardarPersona() {
            if (this.nombrePersona.trim() == "" || this.edadPersona.trim() == "" || this.estaturaPersona.trim() == "") {
                alert("Debe completar todos los campos para registrar sus datos")
            }
            else {
                this.persona.push({
                    nombre: this.nombrePersona,
                    edad: this.edadPersona,
                    estatura: this.estaturaPersona
                })
                this.nombrePersona = ""
                this.edadPersona = ""
                this.estaturaPersona = ""
            }

        },

        buscarNombre(){
            if(this.buscaNombrePersona.trim()==""){
                alert("Debe llenar el campo para buscar una persona")
            }
            else{
                this.indicePersona = this.persona.indexOf(this.buscaNombrePersona)
            }
        }
    }

})