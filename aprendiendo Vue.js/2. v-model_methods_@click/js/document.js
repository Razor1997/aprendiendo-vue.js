
const VModel = new Vue({
    el: "#model1",
    data: {
        game: [
            { nombreJuego: "Age Emperies Conquetors", fecha: "1998", peso: "1.5 GB" },
            { nombreJuego: "Dark Souls II", fecha: "2014", peso: "8 GB" },
            { nombreJuego: "GTA V", fecha: "2013", peso: "92 GB" },
        ],
        agregarNombre: "",
        agregarFecha: "",
        agregarPeso: "",
    },
    methods: {
        agregarJuego() {
            if (this.agregarNombre.trim() == "" || this.agregarFecha.trim() == "" || this.agregarPeso.trim() == ""
            ) {
                alert("Llene todos los campos")
            } else {
                this.game.push({
                    nombreJuego: this.agregarNombre,
                    fecha: this.agregarFecha,
                    peso: this.agregarPeso,
                }),
                    this.agregarNombre = ""
                this.agregarFecha = ""
                this.agregarPeso = ""
            }
        },

    },
});
