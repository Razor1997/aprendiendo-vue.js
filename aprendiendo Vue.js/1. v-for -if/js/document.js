const App = new Vue({
    el: '#app',
    data: {
        computador : [
            { modelo: "Acer Pretador Helios 300", precio: "4'500.000", cantidad: 2 },
            { modelo: "Lenovo Legion Y540", precio: "3'500.000", cantidad: 1 },
            { modelo: "Asus Rog Strix Gaming", precio: "5'000.000", cantidad: 0 },
            { modelo: "MSI G300 Gaming ", precio: "5'000.000", cantidad: 1 }
        ],
        total:0,
    },
    computed: { 
        totalComputadores(){
            this.total=0
            for(compu of this.computador){
                this.total=this.total+compu.cantidad
            }
            return this.total;
        }

    }
})

